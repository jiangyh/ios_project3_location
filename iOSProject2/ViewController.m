//
//  ViewController.m
//  iOSProject2
//
//  Created by Yihao Jiang on 26/5/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import "ViewController.h"
#import "HelpClass.h"
@interface ViewController ()

@end

@implementation ViewController{
    CLLocationManager * locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@synthesize latitudeValueLabel;
@synthesize longitudeValueLabel;
@synthesize addressLabel;
@synthesize altitudeLabel;
@synthesize speedLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    // Send location updates to the current object.
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    // Request authorization, if needed.
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    
    if(authorizationStatus==kCLAuthorizationStatusNotDetermined){
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
    
    
    UIImage * background = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/2.jpg"] scaledToSize:self.view.frame.size] ;
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
//    self.tabBarItem.image = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/GPS.png"] scaledToSize:CGSizeMake(45, 45)] ;
    
}

-(UITabBarItem *)tabBarItem
{
    UITabBarItem *tabBar = [[UITabBarItem alloc] initWithTitle:nil
                                         image:[HelpClass imageWithImage:[UIImage imageNamed:@"./image/GPS.png"] scaledToSize:CGSizeMake(45, 45)]
                                           tag:0];
    [tabBar setSelectedImage: [HelpClass imageWithImage:[UIImage imageNamed:@"./image/GPS.png"] scaledToSize:CGSizeMake(60, 60)]];
    return tabBar;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)getCurrentLocation:(id)sender {
        NSLog(@"Fetch location data");
    
    [locationManager startUpdatingLocation];
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                        message:@"Failed to Get Your Location"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) { NSLog(@"Cancel"); }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) { NSLog(@"OK"); }];
    [errorAlert addAction:cancelAction];
    [errorAlert addAction:okAction];
    
    [self presentViewController:errorAlert animated:YES completion:nil];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"didUpdateToLocation: %@", [locations lastObject]);
    CLLocation *currentLocation = [locations lastObject];

    if (currentLocation != nil) {
        latitudeValueLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        longitudeValueLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        altitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.altitude];
        speedLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.speed];
        
    }else{
        latitudeValueLabel.text = @"nil";
        longitudeValueLabel.text = @"nil";
    }

    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil&& [placemarks count] >0) {
            placemark = [placemarks lastObject];
            NSArray *lines = placemark.addressDictionary[ @"FormattedAddressLines"];
            NSString *addressString = [lines componentsJoinedByString:@"\n"];
            addressLabel.text = addressString;
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    }];
    
    
    // Turn off the location manager to save power.
    //[locationManager stopUpdatingLocation];
}


@end
