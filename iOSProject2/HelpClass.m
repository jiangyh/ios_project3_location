//
//  HelpClass.m
//  iOSProject2
//
//  Created by Zhen on 4/6/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelpClass.h"

@implementation HelpClass

//This help class used to reset the size of image
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
