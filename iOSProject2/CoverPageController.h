//
//  CoverPageController.h
//  iOSProject2
//
//  Created by Zhen on 5/6/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpClass.h"

@interface CoverPageController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;

@property (weak, nonatomic) IBOutlet UIImageView *sharingImage;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *socialLabel;
@end
