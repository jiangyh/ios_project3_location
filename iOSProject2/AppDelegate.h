//
//  AppDelegate.h
//  iOSProject2
//
//  Created by Yihao Jiang on 26/5/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

