//
//  ShareViewController.m
//  iOSProject2
//
//  Created by Zhen on 27/5/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import "ShareViewController.h"
#import <Social/Social.h>
#import "HelpClass.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

@synthesize shareURL;
@synthesize imageView;
bool isFullScreen;
@synthesize currentImage;
@synthesize originalPosition;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Change the background of this view
    originalPosition = self.imageView.frame;
    UIImage * background = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/4.jpg"] scaledToSize:self.view.frame.size] ;
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
}

//Set tabBar image and selected image
-(UITabBarItem *)tabBarItem
{
    UITabBarItem *tabBar = [[UITabBarItem alloc] initWithTitle:nil
                                         image:[HelpClass imageWithImage:[UIImage imageNamed:@"./image/Twitter.png"] scaledToSize:CGSizeMake(45, 45)]
                                           tag:2];
    [tabBar setSelectedImage: [HelpClass imageWithImage:[UIImage imageNamed:@"./image/Twitter.png"] scaledToSize:CGSizeMake(60, 60)]];
    return tabBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Used to set text, image, URL to the twitter sheet and perform the sharing action
- (IBAction)shareButton:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *twitterSheet =
        [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [twitterSheet setInitialText:@"Hello, world!"];
        [twitterSheet addURL:[NSURL URLWithString:shareURL.text]];
        [twitterSheet addImage:currentImage];
        [self presentViewController:twitterSheet animated:YES completion:nil];
    }
    else
    {//If user's twitter account has not been connected to the smart phone, pop up an alertView
        
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Sorry"
                                                                            message:@"Please connect your device with Twitter account first!"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) { NSLog(@"Cancel"); }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) { NSLog(@"OK"); }];
        [errorAlert addAction:cancelAction];
        [errorAlert addAction:okAction];

        [self presentViewController:errorAlert animated:YES completion:nil];
    }
    
}

// Choose image from album or camera to share
- (IBAction)chooseImage:(id)sender {
    // Create pop up for selecting image
    UIAlertController *sheet = [UIAlertController alertControllerWithTitle:@"Image Source"
                                                                   message:@"Please Choose"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    // Camera
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                                                       imagePickerController.delegate = self;
                                                       imagePickerController.allowsEditing = YES;
                                                       imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                       [self presentViewController:imagePickerController animated:YES completion:^{}];
                                                   }];
    // Photo Roll
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Photo Roll" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                                                          imagePickerController.delegate = self;
                                                          imagePickerController.allowsEditing = YES;
                                                          imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                          [self presentViewController:imagePickerController animated:YES completion:^{}];
                                                      }];
    // Cancel
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    
    // Verify if it support camera
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        
    {
        [sheet addAction:camera];
        [sheet addAction:photoRoll];
        [sheet addAction:cancel];
    }
    else //if not just select from album
    {
        [sheet addAction:photoRoll];
        [sheet addAction:cancel];
    }
    
    [self presentViewController:sheet animated:YES completion:nil];
    
    //sheet2.tag = 255;
    //[sheet2 showInView: self.view];
}


#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{//Close picker veiw and put preview of the image on screen
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    currentImage = image;
    
    isFullScreen = NO;
    [self.imageView setImage:image];
    
    self.imageView.tag = 100;
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{//Dismiss view when cancel
    [self dismissViewControllerAnimated:YES completion:^{}];
}

//Set touch event to zoom out the preview image
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    isFullScreen = !isFullScreen;
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self.view];
    
    CGPoint imagePoint = self.imageView.frame.origin;
    //touchPoint.x ，touchPoint.y is touching point
    
    // When touch inside the imageView, zoom out. Touch again zoom in back
    if(imagePoint.x <= touchPoint.x && imagePoint.x +self.imageView.frame.size.width >=touchPoint.x && imagePoint.y <=  touchPoint.y && imagePoint.y+self.imageView.frame.size.height >= touchPoint.y)
    {
        // Set zoom out animation
        [UIView beginAnimations:nil context:nil];
        // Animation time
        [UIView setAnimationDuration:1];
        
        if (isFullScreen) {
            // Zoom out
            
            self.imageView.frame = CGRectMake(0, 0, 320, 480);
        }
        else {
            // Zoom in
            self.imageView.frame = originalPosition;
        }
        
        // commit animations
        [UIView commitAnimations];
        
    }
    
}


@end
