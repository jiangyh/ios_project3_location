//
//  main.m
//  iOSProject2
//
//  Created by Yihao Jiang on 26/5/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
