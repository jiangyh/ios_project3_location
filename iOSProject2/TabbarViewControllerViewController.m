//
//  TabbarViewCOntrollerViewController.m
//  iOSProject2
//
//  Created by Zhen on 6/6/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import "TabbarViewControllerViewController.h"
#import "CoverPageController.h"
#import "ViewController.h"
#import "ShareViewController.h"
#import "HelpClass.h"

@interface TabbarViewControllerViewController ()

@end

@implementation TabbarViewControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Set home page as initial page
    [self setSelectedIndex:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
