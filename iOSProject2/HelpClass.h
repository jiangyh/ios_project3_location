//
//  HelpClass.h
//  iOSProject2
//
//  Created by Zhen on 4/6/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface HelpClass : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
