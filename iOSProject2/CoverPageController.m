//
//  CoverPageController.m
//  iOSProject2
//
//  Created by Zhen on 5/6/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import "CoverPageController.h"
#import "ViewController.h"

@interface CoverPageController ()

@end

@implementation CoverPageController

@synthesize locationImage;
@synthesize sharingImage;
@synthesize locationLabel;
@synthesize socialLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Set background image
    CGSize screenSize = self.view.frame.size;
    
    UIImage * background = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/1.jpg"] scaledToSize:screenSize];
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
    
    // Set locaiton image
    CGSize imageSize = CGSizeMake(screenSize.width/3, screenSize.width/3);
    UIImage * locationbackground = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/2.jpg"] scaledToSize:imageSize];
    locationImage.backgroundColor = [UIColor colorWithPatternImage:locationbackground];
    locationImage.frame = CGRectMake(10,
                                     imageSize.height*2,
                                     imageSize.width,
                                     imageSize.width
                                     );
    locationImage.layer.cornerRadius = imageSize.width/2;
    
    // Set sharing image
    UIImage * sharingbackground = [HelpClass imageWithImage:[UIImage imageNamed:@"./image/4.jpg"] scaledToSize:imageSize];
    sharingImage.backgroundColor = [UIColor colorWithPatternImage:sharingbackground];
    sharingImage.frame = CGRectMake(imageSize.width*2-10,
                                     imageSize.height*2,
                                     imageSize.width,
                                     imageSize.width
                                     );
    sharingImage.layer.cornerRadius = imageSize.width/2;
    
    // Set locaitonLabel position
    locationLabel.frame = CGRectMake(10,
                                     imageSize.height*2.4,
                                     imageSize.width,
                                     imageSize.width
                                     );
    
    // Set socialLabel position
    socialLabel.frame = CGRectMake(imageSize.width*2-10,
                                   imageSize.height*2.4,
                                   imageSize.width,
                                   imageSize.width
                                   );
    
}

// Set tabBar image and seleted image
-(UITabBarItem *)tabBarItem
{
    UITabBarItem *tabBar = [[UITabBarItem alloc] initWithTitle:nil
                                         image:[HelpClass imageWithImage:[UIImage imageNamed:@"./image/home.png"] scaledToSize:CGSizeMake(45, 45)]
                                           tag:1];
    [tabBar setSelectedImage: [HelpClass imageWithImage:[UIImage imageNamed:@"./image/home.png"] scaledToSize:CGSizeMake(60, 60)]];
    return tabBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
