//
//  ShareViewController.h
//  iOSProject2
//
//  Created by Zhen on 27/5/17.
//  Copyright © 2017 Yihao Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIApplicationDelegate>
@property (weak, nonatomic) IBOutlet UITextView *shareURL;
- (IBAction)shareButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)chooseImage:(id)sender;
@property UIImage *currentImage;
@property CGRect originalPosition;
//+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
